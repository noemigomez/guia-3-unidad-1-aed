#include <iostream>
using namespace std;

#include "Programa.h"
#include "Lista.h"

Lista::Lista() {}

//ordena la lista, modo burbuja
void Lista::ordenar(){
  Nodo *temp_actual = this->raiz;
  // variable para el cambio al ordenar
  int var_orden;
  // para recorrer la lista hasta el final, final NULL
  while(temp_actual != NULL){
    // tomamos el nodo siguiente al que tenemos como actual para comparar
    Nodo *temp_siguiente = temp_actual->sig;
    // el siguiente no es el último
    while(temp_siguiente != NULL){
      // si el dato actual es menor al que le sigue
      if(temp_actual->numero->get_numero() > temp_siguiente->numero->get_numero()){
        /*variables temporales son punteros,
        usamos una auxiliar var_orden para intercambiar los valores */
        var_orden = temp_siguiente->numero->get_numero();
        // el siguiente será el temporal actual ya que es menor
        temp_siguiente->numero->set_numero(temp_actual->numero->get_numero());
        // el actual será el siguiente ya que es mayor
        temp_actual->numero->set_numero(var_orden);
      }
      // el siguiente del siguiente para seguir con el ciclo
      temp_siguiente = temp_siguiente->sig;
    }
    // el siguiente del que evaluamos para continuar el ciclo
    temp_actual = temp_actual->sig;
  }
}

void Lista::add_numero (Numero *numero) {
    Nodo *tmp;

    // crea un nodo
    tmp = new Nodo;
    // asigna número
    tmp->numero = numero;
    // null por defecto
    tmp->sig = NULL;

    //si es el primer nodo queda como raíz y último
    if (this->raiz == NULL) {
        this->raiz = tmp;
        this->ultimo = this->raiz;
    //sino apunta al último nodo y deja el nuevo como último
    }else{
      this->ultimo->sig = tmp;
      this->ultimo = tmp;
    }
}

void Lista::imprimir () {
  // para recorrer la lista
  Nodo *tmp = this->raiz;

  // recorre hasta que llege al final, null
  cout << "LISTA DE NÚMEROS" << endl;
  while (tmp != NULL) {
      cout << "|" << tmp->numero->get_numero() << "|";
      tmp = tmp->sig;
  }
  cout << "\n";
}
