#include <iostream>
using namespace std;

#ifndef NUMERO_H
#define NUMERO_H

class Numero {
    private:
        // número
        int numero = 0;

    public:
        //constructor
        Numero(int numero);

        //metodos
        int get_numero();
        void set_numero(int numero);
};
#endif
