#include <iostream>
using namespace std;

// estructura nodo
#include "Programa.h"

// clases
#include "Numero.h"
#include "Lista.h"

// clase programa
class Programa {
    private:
        // lista privada
        Lista *lista = NULL;

    public:
        /* constructor */
        Programa() {
            this->lista = new Lista();
        }
        // retorna la lista
        Lista *get_lista() {
            return this->lista;
        }
};

// procedimiento que se encarga de solicitar números para agregar a la lista
void solicitud(Lista *lista)
{
  // variables de si desea agregar o no y lo que, en caso de que si, va a agregar
  string opcion;
  int agregar;

  cout << "¿Desea agregar un número a la lista? (0 para sí, otro caracter para no): ";
  cin >> opcion;

  // si la opcion es 0 como se solicitó se agrega un número
  if(opcion=="0"){
    cout << "Ingrese número a agregar: ";
    cin >> agregar;
    // se envía a función propia de la lista que agrega el número
    lista->add_numero(new Numero(agregar));
    // recursividad para que siga agregando números
    lista->ordenar();
    lista->imprimir();
    solicitud(lista);
  }
  // sino, la lista se ordena, se imprime y termina el programa
  else{
    lista->ordenar();
    lista->imprimir();
    cout << "Vuelva pronto, mil besos." << endl;
  }
}

// función principal
int main (void) {
    // crea programa y extrae la lista
    Programa p = Programa();
    Lista *lista = p.get_lista();

    // envío a solicitar datos
    solicitud(lista);

    return 0;
}
