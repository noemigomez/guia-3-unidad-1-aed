# Creador de lista de números ordenada

El programa solicita tantos números como desee el usuario y los va agregando a una lista que se ordena y se muestra al final.

## Comenzando

Comienza preguntando al usuario si desea agregar o no un número a la lista. Si la respuesta a esta pregunta es afirmativa se le solicita el ingreso del número que desea añadir y posterior a su ingreso vuelve a hacer la misma pregunta, así sucesivamente hasta que no desee agregar más números. Si la respuesta a la pregunta es negativa entonces el programa se detiene y muestra la lista ordenada de manera creciente.

## Prerrequisitos

### Sistema Operativo

Para ejecutar el programa se necesita un sistema operativo Linux, de preferencia Ubuntu o Debian.

### Make

Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:

`sudo apt install make`

**En caso de no usar make**

Se puede ejecutar el programa sin make (revisar **Ejecutando las pruebas**).

## Ejecutando las pruebas

### Iniciar programa "compilar"

**Con make**

En la terminal situada en la carpeta con nuestros archivos se debe ejecutar el comando:

`make`

**Sin make**

En la terminar situada en la carpeta con nuestros archivos se debe ejecutar el siguiente comando:

`g++ Programa.cpp Lista.cpp Numero.cpp -o programa`

### Luego de compilar

Para ejecutar el programa una vez compilado se debe ejecutar el siguiente comando:

`./programa`

Al ejecutar dicho comando comienza a correr el programa. Comienza preguntando si el usuario desea o no agregar un número a la lista y para poder añadir un número debe ingresar la opción "0". El ingreso de cualquier otro carácter que no sea 0 simboliza la detención automática del programa.

Si la respuesta a la pregunta fue afirmativa, ingresó 0, entonces el programa ahora solicita el ingreso del número que desea agregar a la lista. Este número debe ser un número entero (negativos, positivos y el cero), el ingreso de cualquier otro tipo simboliza el ingreso del número 0 y el término automático del programa. Al finalizar e ingresar el número el programa vuele a hacer la pregunta inicial. Esto se repite constantemente hasta que su respuesta a la pregunta inicial sea otro carácter distinto de 0, o sea, que la respuesta sea negativa. Por cada ingreso de un número a la lista el estado de esta será mostrado.

Si la respuesta es negativa, ingresó otro carácter distinto de 0, el programa se detiene, ordena la lista de manera creciente y finalmente muestra la lista final.

## Despliegue

La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux (señalado en Prerrequisitos).

## Construido con
- Lenguaje c++: librería iostream.

## Autor
- Noemí Gómez Rodríguez - nogomez19@alumnos.utalca.cl
