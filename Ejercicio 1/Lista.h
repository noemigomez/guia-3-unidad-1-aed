#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;
        Nodo *comienzo = NULL;

    public:
        //constructor
        Lista();
        // ordena la lista
        void ordenar();
        // agrega número y crea nuevo nodo recibiendo una clase Numero
        void add_numero (Numero *numero);
        //muestra la lista
        void imprimir ();
};
#endif
