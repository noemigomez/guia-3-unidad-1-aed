#include "Numero.h"

// constructor
Numero::Numero(int numero) {
    // define número
    this->numero = numero;
}

// metodo get para retornar el número
int Numero::get_numero() {
    return this->numero;
}

// método set para cambiar el número
void Numero::set_numero(int numero){
    this->numero = numero;
}
