#include "Dato.h"

/* define la estructura del nodo. */
typedef struct _Nodo {
    Dato *dato;
    struct _Nodo *sig;
} Nodo;
