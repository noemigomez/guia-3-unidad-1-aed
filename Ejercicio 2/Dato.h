#include <iostream>
using namespace std;

#ifndef DATO_H
#define DATO_H

class Dato {
    private:
        string dato = "\0";

    public:
        //constructor
        Dato(string dato);

        //metodos
        string get_dato();
};
#endif
