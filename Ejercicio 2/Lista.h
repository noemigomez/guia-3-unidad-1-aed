#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        //constructor
        Lista();

        // agrega número y crea nuevo nodo recibiendo una clase Numero
        void add_dato (Dato *dato);
        //muestra la lista
        void imprimir ();
        void agregar_lista_a_otra(Lista *list);
};
#endif
