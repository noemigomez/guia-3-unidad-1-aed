#include <iostream>
using namespace std;

#include "Programa.h"
#include "Lista.h"

Lista::Lista() {}

void Lista::add_dato (Dato *dato) {
    Nodo *tmp;

    // crea un nodo
    tmp = new Nodo;
    // asigna número
    tmp->dato = dato;
    // null por defecto
    tmp->sig = NULL;

    //si es el primer nodo queda como raíz y último
    if (this->raiz == NULL) {
        this->raiz = tmp;
        this->ultimo = this->raiz;
    //sino apunta al último nodo y deja el nuevo como último
    }else{
      this->ultimo->sig = tmp;
      this->ultimo = tmp;
    }
}

void Lista::imprimir () {
  // para recorrer la lista
  Nodo *tmp = this->raiz;

  // recorre hasta que llege al final, null
  while (tmp != NULL) {
      cout << "[" << tmp->dato->get_dato() << "]";
      tmp = tmp->sig;
  }
  cout << "\n";
}

// función que agrega datos a una lista sacandolos de otra ya creada
void Lista::agregar_lista_a_otra(Lista *list){
  // para recorrer la lista
  Nodo *tmp = list->raiz;

  // recorre hasta que llege al final, null
  while (tmp != NULL) {
      // agrega dato de la lista revisada a la lista que llamó la función
      add_dato(tmp->dato);
      tmp = tmp->sig;

  }
}
