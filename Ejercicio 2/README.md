# Enlazador de listas

El programa permite la generación de dos listas con datos ingresados por el usuario y, finalmente, crea una nueva lista con los elementos de las dos creadas.

## Comenzando

Lo primero que hace el programa es crear dos listas vacías, posteriormente con el programa este pregunta si el usuario desea agregar un elemento a la lista o no. Si la respuesta es afirmativa entonces pregunta a que lista desea agregar los datos. Cuando especifique a que lista desea agregar el elemento se solicita que elemento es el que desea agregar. Al finalizar esto el programa vuelve a preguntar si desea o no agregar un elemento a alguna de las listas; esto se repite hasta que al preguntar si desea añadir un elemento el usuario ingrese que no. Cuando la respuesta a la pregunta es negativa el programa se detiene e imprime las 3 listas, ambas a las que el usuario a agregado datos continuamente y la que contiene los datos de ambas.

## Prerrequisitos

### Sistema Operativo

Para ejecutar el programa se necesita un sistema operativo Linux, de preferencia Ubuntu o Debian.

### Make

Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:

`sudo apt install make`

**En caso de no usar make**

Se puede ejecutar el programa sin make (revisar **Ejecutando las pruebas**).

## Ejecutando las pruebas

### Iniciar programa "compilar"

**Con make**

En la terminal situada en la carpeta con nuestros archivos se debe ejecutar el comando:

`make`

**Sin make**

En la terminar situada en la carpeta con nuestros archivos se debe ejecutar el siguiente comando:

`g++ Programa.cpp Lista.cpp Dato.cpp -o programa`

### Luego de compilar

Para ejecutar el programa una vez compilado se debe ejecutar el siguiente comando:

`./programa`

Cuando el programa comienza a correr lo primero que realiza es crear ambas listas iniciales vacías, luego pregunta es si desea o no agregar un elemento a la lista. Para agregar un elemento a la lista se debe ingresar "0", cualquier otro ingreso que no sea este el programa finalizará e imprimirá las listas como están. 

Si la respuesta a querer ingresar un elemento a la lista es afirmativa, ingresa "0", se hará una segunda pregunta que es cual de las dos listas creadas en un inicio desea añadir el elemento. Estos deben ser 1 o 2, el ingreso de otro dato que no sea uno de estas opciones significa el fin del programa. Al seleccionar a lista el programa procede a la solicitud del nombre del elemento que desea agregar, este puede ser cualquier tipo de dato. Al ingresar finalmente el elemento se volverá a preguntar si desea agregar un elemento a alguna de las listas, esto se repite hasta que responda esta pregunta de manera negativa.

Si la respuesta a querer ingresar un elemento a la lista es negativa, ingresa otra opción que no es "0", el programa imprime ambas listas y la lista nueva formada por los elementos de las dos terminando de esta manera y cerrando el programa.

## Despliegue

La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux (señalado en Prerrequisitos).

## Construido con
- Lenguaje c++: librería iostream.

## Autor
- Noemí Gómez Rodríguez - nogomez19@alumnos.utalca.cl
