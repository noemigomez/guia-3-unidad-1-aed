#include <iostream>
using namespace std;

/* definición de la estructura Nodo. */
#include "Programa.h"

/* clases */
#include "Dato.h"
#include "Lista.h"

// clase programa
class Programa {
  private:
    // lista privada
    Lista *lista = NULL;

  public:
    /* constructor */
    Programa() {
      this->lista = new Lista();
    }

    // retorna lista
    Lista *get_lista() {
      return this->lista;
    }
};

// procedimiento que mezcla las listas
void mezlcar_listas(Lista *lista_1, Lista *lista_2, Lista *lista_3){
  // se llama dos veces para agregar cada lista a la lista nueva
  lista_3->agregar_lista_a_otra(lista_1);
  lista_3->agregar_lista_a_otra(lista_2);

  // se imprimen las listas
  cout << "LISTA 1: ";
  lista_1->imprimir();
  cout << "LISTA 2: ";
  lista_2->imprimir();
  cout << "LISTA 3: ";
  lista_3->imprimir();
}

// procedimiento que solicita el dato a agregar a la lista
void solicitar_dato(Lista *lista){
  string agregar;
  cout << "Dato a agregar: ";
  cin >> agregar;

  // se agrega dato a la lista
  lista->add_dato(new Dato(agregar));
}

// procedimiento que agrega datos a ambas listas
void agregar_datos_listas(Lista *lista_1, Lista *lista_2)
{
  // variables opcion para saber si quiere agregar algo a la lista y list para saber a que lista
  string opcion;
  int list;
  cout << "¿Desea agregar dato a una lista? (0 para SÍ, otro para no): ";
  cin >> opcion;

  // 0 para sí
  if(opcion=="0"){
    cout << "¿A qué lista desea agregar datos, 1 o 2?: ";
    cin >> list;

    // llamada a procedimiento que agrega un dato a una lista según la lista que se seleccione
    switch (list) {
      case 1:
      solicitar_dato(lista_1);
      break;

      case 2:
      solicitar_dato(lista_2);
      break;

      // cualquier valor que no sea 1 o 2
      default:
      cout << "Opción incorrecta, vuelva a intentarlo." << endl;
    }
    // recursividad para que agregue datos hasta que señale que no quiere agregar más
    agregar_datos_listas(lista_1, lista_2);
  // cualquier otra cosa para no
  }else{

    // cuando no se desee agregar más se crea la lista 3
    Programa p3 = Programa();
    Lista *lista_3 = p3.get_lista();

    // procedimiento que mezcla las listas
    mezlcar_listas(lista_1, lista_2, lista_3);

    cout << "Adiós." << endl;
  }
}

// función principal
int main(void)
{
  /* Se crean distintos p de Programa debido a que si igualamos dos variables de tipo Lista
    del mismo Programa serán la misma lista (listas definidas con punteros)  */
  Programa p1 = Programa();
  Programa p2 = Programa();
  Lista *lista_1 = p1.get_lista();
  Lista *lista_2 = p2.get_lista();

  agregar_datos_listas(lista_1, lista_2);

}
