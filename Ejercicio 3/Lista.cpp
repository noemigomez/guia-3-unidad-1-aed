#include <iostream>
using namespace std;

#include "Programa.h"
#include "Lista.h"

// constructor
Lista::Lista() {}

//ordena la lista, modo burbuja
void Lista::ordenar(){
  Nodo *temp_actual = this->raiz;
  // variable para el cambio al ordenar
  int var_orden;
  // para recorrer la lista hasta el final, final NULL
  while(temp_actual != NULL){
    // tomamos el nodo siguiente al que tenemos como actual para comparar
    Nodo *temp_siguiente = temp_actual->sig;
    // el siguiente no es el último
    while(temp_siguiente != NULL){
      // si el dato actual es menor al que le sigue
      if(temp_actual->dato > temp_siguiente->dato){
        /*variables temporales son punteros,
        usamos una auxiliar var_orden para intercambiar los valores */
        var_orden = temp_siguiente->dato;
        temp_siguiente->dato = temp_actual->dato;
        temp_actual->dato = var_orden;
      }
      // el siguiente del siguiente para seguir con el ciclo
      temp_siguiente = temp_siguiente->sig;
    }
    // el siguiente del que evaluamos para continuar el ciclo
    temp_actual = temp_actual->sig;
  }
}

// agrega el dato a la lista
void Lista::add_dato (int dato) {
    Nodo *tmp;
    // crea un nodo
    tmp = new Nodo;
    // asigna número
    tmp->dato = dato;
    // null por defecto
    tmp->sig = NULL;

    //si es el primer nodo queda como raíz y último
    if (this->raiz == NULL) {
        this->raiz = tmp;
        this->ultimo = this->raiz;
    //sino apunta al último nodo y deja el nuevo como último
    }else{
      this->ultimo->sig = tmp;
      this->ultimo = tmp;
    }
}

// imprime la lista
void Lista::imprimir () {
  // para recorrer la lista
  Nodo *tmp = this->raiz;

  // recorre hasta que llege al final, null
  while (tmp != NULL) {
      cout << "[" << tmp->dato << "]";
      tmp = tmp->sig;
  }
  cout << "\n";
}

// agrega un número creando un nuevo nodo
void Lista::agregar_numeros(Nodo *tmp){
  // se crea el nuevo nodo de la lista
  Nodo *nuevo = new Nodo;

  /*asignación de datos:
    el dato del nuevo nodo será el sucesor del que llega
    el siguiente del nuevo dato será el que antes seguía al nodo que llega
    el siguiente del nodo que llega será el nuevo ya que es su sucesor*/
  nuevo->dato = tmp->dato + 1;
  nuevo->sig = tmp->sig;
  tmp->sig = nuevo;
}

/*función que guarda la distancia de cada nodo hasta el siguiente
  esto sirve para saber cuantos numeros se debe agregar*/
void Lista::evaluar_distancia(){
  // nodo para recorrer lista
  Nodo *tmp = this->raiz;
  // hasta que sea el último
  while(tmp!=NULL){
    // nodo que le sigue
    Nodo *tmp_sig = tmp->sig;
    // si el nodo que le sigue existe
    if(tmp_sig!=NULL){
      // se restan dos números consecutivos para saber por cuanto se separan
      tmp->distancia = tmp_sig->dato - tmp->dato;
    }
    // para continuar con el ciclo
    tmp = tmp->sig;
  }
}

// procedimiento que evalua la lista para llenarla
void Lista::llenar(){
  // para recorrer la lista
  Nodo *tmp = this->raiz;

  // se determinan las distancias entre los números de la lista
  evaluar_distancia();

  // ciclo que recorre la lista
  for(int i=1; tmp!=NULL; i++){
    /*si la distancia es mayor a uno significa que no tiene un número consecutivo que le sigue
    por lo tanto, se agrega un número*/
    if(tmp->distancia > 1){
      agregar_numeros(tmp);
    }
    tmp = tmp->sig;
    /*se vuelven a evaluar las nuevas distancias para seguir con el ciclo
    esto sirve porque si uno fue agregado y aún faltan números se agreguen los que faltan*/
    evaluar_distancia();
  }
}
