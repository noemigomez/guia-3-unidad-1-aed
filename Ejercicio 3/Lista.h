#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;


    public:
        //constructor
        Lista();


        //ordena la lista de manera creciente
        void ordenar();
        // agrega número y crea nuevo nodo recibiendo una entero
        void add_dato (int dato);
        //muestra la lista
        void imprimir ();
        // agrega el número dentro de la lista
        void agregar_numeros(Nodo *tmp);
        // determina la distancia entre los nodos
        void evaluar_distancia();
        //llena la lista con los números que faltan
        void llenar();
};
#endif
