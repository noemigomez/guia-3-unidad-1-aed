# Ejercicio

El programa crea una lista de números que se llena mediante solicitud al usuario. El programa llena la lista, la ordena de manera creciente y si los números no son correlativos, por ejemplo 10-11-16-17 (11 y 16 no son correlativos), entonces el programa se encarga de agregar números dentro de la lista de manera que la lista tenga solo números consecutivos y almacene todos los números desde el primer nodo hasta el último nodo.

## Comenzando

Al correr el programa lo primero que realiza es crear la lista vacía para comenzar a llenarla. Posterior a esto se solicita al usuario si desea o no ingresar un número a la lista, si la respuesta es afirmativa entonces pregunta cual es el número que desea agregar y vuelve a preguntar si desea agregar un número a la lista, así sucesivamente hasta que la respuesta a esa pregunta sea negativa. Cuando no se deseen agregar más números el programa ordena la lista de manera creciente y la llena de manera que almacene todos los números desde el primer nodo hasta el último. Imprime la lista antes y después finalizando así el programa.

## Prerrequisitos

### Sistema Operativo

Para ejecutar el programa se necesita un sistema operativo Linux, de preferencia Ubuntu o Debian.

### Make

Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:

`sudo apt install make`

**En caso de no usar make**

Se puede ejecutar el programa sin make (revisar **Ejecutando las pruebas**).

## Ejecutando las pruebas

### Iniciar programa "compilar"

**Con make**

En la terminal situada en la carpeta con nuestros archivos se debe ejecutar el comando:

`make`

**Sin make**

En la terminar situada en la carpeta con nuestros archivos se debe ejecutar el siguiente comando:

`g++ Programa.cpp Lista.cpp -o programa`

### Luego de compilar

Para ejecutar el programa una vez compilado se debe ejecutar el siguiente comando:

`./programa`

Cuando el programa comienza a correr lo primero que hace es crear la lista vacía. Luego pregunta al usuario si desea o no agregar un elemento a la lista, donde la respuesta afirmativa está dada por el ingreso del caracter "0". El ingreso de cualquier caracter que no sea 0, incluso si es 00, entonces el programa finaliza.

Si la respuesta es afirmativa a agregar un número a la lista, ingresa "0", el programa procede a preguntarle al usuario cual es el número que desea agregar. Este número corresponde a un número entero (negativos, positivos y el 0), el ingreso de un dato que no sea entero se añade un 0 y simboliza el fin del programa imprimiendo todo tal y como está hasta ese momento. Una vez ingresado el número a agregar el programa vuelve a preguntar si desea o no agregar un número a la lista y este ciclo se detiene cuando la respuesta a la pregunta inicial sea negativa.

Si la respuesta es negativa a agregar un número a la lista, ingresa otro caracter que no sea "0", entonces el programa procede a imprimir la lista ordenada de manera creciente tal y como el usuario la ingresó y también imprime la lista con los números agregados almacenando todos los números desde el primer nodo hasta el último. Por lo tanto, termina el programa.

## Despliegue

La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux (señalado en Prerrequisitos).

## Construido con
- Lenguaje c++: librería iostream.

## Autor
- Noemí Gómez Rodríguez - nogomez19@alumnos.utalca.cl
