#include <iostream>
using namespace std;

/* definición de la estructura Nodo. */
#include "Programa.h"

/* clases */
#include "Lista.h"

// clase programa
class Programa {
  private:
    Lista *lista = NULL;

  public:
    /* constructor */
    Programa() {
      this->lista = new Lista();
    }
    // devuelve lista
    Lista *get_lista() {
      return this->lista;
    }
};

// procedimiento que solicita y agrega el dato a la lista
void solicitar_dato(Lista *lista){
  // variable de número a agregar
  int agregar;
  cout << "Dato a agregar: ";
  cin >> agregar;

  // se agrega el número
  lista->add_dato(agregar);
}

// procedimiento que agrega datos a la lista
void agregar_dato_lista(Lista *lista)
{
  // variable por si quiere o no agregar algo
  string opcion;
  cout << "¿Desea agregar dato a una lista? (0 para SÍ, otro para no): ";
  cin >> opcion;

  // 0 para sí
  if(opcion=="0"){
    /*se solicita el dato y se vuelve a llamar a la función de manera recursiva
      por si quiere agregar más datos*/
    solicitar_dato(lista);
    agregar_dato_lista(lista);
  // otro para no
  }else{
    // se ordena la lista
    lista->ordenar();
    // se imprime como está
    lista->imprimir();
    // se llena la lista con los números que faltar y se vuelve a imprimir
    lista->llenar();
    lista->imprimir();
  }
}

// función principal
int main(void)
{
  // creación programa y extracción de lista
  Programa p = Programa();
  Lista *lista = p.get_lista();

  // enviado a agregar datos a la lista
  agregar_dato_lista(lista);
}
