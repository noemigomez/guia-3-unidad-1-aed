
/* define la estructura del nodo. */
typedef struct _Nodo {
    int dato;
    // distancia de un nodo al otro
    int distancia;
    struct _Nodo *sig;
} Nodo;
